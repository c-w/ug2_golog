% ---------------------------------------------------------------------
%  ----- Informatics 2D - 2011/12 - Second Assignment - Planning -----
% ---------------------------------------------------------------------



% --- Cross-file definitions ------------------------------------------

:- multifile connected/3, agent/1, box/1, key/2, at/3.



% --- Primitive control actions ---------------------------------------

primitive_action( move(_) ).
primitive_action( push(_, _, _) ).
primitive_action( pickup(_) ).



% --- Precondition for primitive actions ------------------------------

poss( move(To), S) :-
    (
        at(ag, From, S),
        connected(From, _, To),
        (
            % the agent can move to an empty space
            not(at(_, To, S));
            (
                % or to a space occupied by a key
                at(Key, To, S),
                key(Key, _)
            )
        )
    ).

poss( push(Box, LocBoxFrom, LocBoxTo), S) :-
    (
        at(ag, LocAgent, S),
        box(Box),
        holding(Key, S),
        key(Key, Box),
        at(Box, LocBoxFrom, S),
        connected(LocAgent, Direction, LocBoxFrom),
        connected(LocBoxFrom, Direction, LocBoxTo),
        not(at(_, LocBoxTo, S))
    ).

% the agent can pick up a key if he is at the same location as the key
poss( pickup(Key), S) :-
    (
        key(Key, _),
        at(ag, LocAgent, S),
        at(Key, LocAgent, S)
    ).



% --- Successor state axioms ------------------------------------------

holding(Key, result(A, S)) :-
    (
        key(Key, _),
        (
            % the agent is holding a key if he picked it up
            A=pickup(Key);
            (
                % or if he did not pick it up but was holding it before
                not(A=pickup(Key)),
                holding(Key, S)
            )
        )
    ).

at(Who, Loc, result(A, S)) :-
    (
        (
            agent(Who), 
            (
                A=move(Loc);
                A=push(_, Loc, _);
                (
                    at(Who, Loc, S),
                    not(A=move(_)),
                    not(A=push(_, _, _))
                )
            )
        );
        (
            box(Who),
            (
                A=push(Who, _, Loc);
                (
                    at(Who, Loc, S),
                    not(A=push(Who, _, _))
                )
            )
        );
        (
            % a key stays at the same location if it hasn't been picked up
            key(Who, _),
            (
                at(Who, Loc, S),
                not(A=pickup(Who))
            )
        )
    ).



% ---------------------------------------------------------------------
% ---------------------------------------------------------------------
