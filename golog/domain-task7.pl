% ---------------------------------------------------------------------
%  ----- Informatics 2D - 2011/12 - Second Assignment - Planning -----
% ---------------------------------------------------------------------



% --- Cross-file definitions ------------------------------------------

:- multifile connected/3, agent/1, box/1, at/3.



% --- Primitive control actions ---------------------------------------

primitive_action( move(_) ).
primitive_action( push(_, _, _) ).



% --- Precondition for primitive actions ------------------------------

poss( move(To), S) :-
    (
        at(ag, From, S),
        connected(From, _, To),
        not(at(_, To, S))
    ).

poss( push(Box, LocBoxFrom, LocBoxTo), S) :-
    (
        at(ag, LocAgent, S),
        box(Box),
        at(Box, LocBoxFrom, S),
        connected(LocAgent, Direction, LocBoxFrom),
        connected(LocBoxFrom, Direction, LocBoxTo),
        not(at(_, LocBoxTo, S))
    ).



% --- Successor state axioms ------------------------------------------

odd(ag, result(_, S)) :-
    (
        % after any action, the plan has even length if it had odd length before
        % or it has odd length if it had even length before
        not(odd(ag, S))
    ).

at(Who, Loc, result(A, S)) :-
    (
        (
            agent(Who), 
            (
                A=move(Loc);
                A=push(_, Loc, _);
                (
                    at(Who, Loc, S),
                    not(A=move(_)),
                    not(A=push(_, _, _))
                )
            )
        );
        (
            box(Who),
            (
                A=push(Who, _, Loc);
                (
                    at(Who, Loc, S),
                    not(A=push(Who, _, _))
                )
            )
        )
    ).



% ---------------------------------------------------------------------
% ---------------------------------------------------------------------
