% ---------------------------------------------------------------------
%  ----- Informatics 2D - 2011/12 - Second Assignment - Planning -----
% ---------------------------------------------------------------------
% not(odd(ag, S)) - Terminates in 11.464s
%  1 move(loc3-3)
%  2 push(ba, loc2-3, loc1-3)
%  3 move(loc2-4)
%  4 move(loc1-4)
%  5 push(ba, loc1-3, loc1-2)
%  6 move(loc2-3)
%  7 move(loc3-3)
%  8 move(loc3-2)
%  9 move(loc3-1)
% 10 push(bc, loc2-1, loc1-1)
% 11 push(bb, loc2-2, loc2-3)
% 12 move(loc3-2)
% 13 move(loc3-3)
% 14 push(bb, loc2-3, loc1-3)
%
% odd(ag, S) - Terminates in 30.282s
%  1 move(loc3-3)
%  2 push(ba, loc2-3, loc1-3)
%  3 move(loc2-4)
%  4 move(loc1-4)
%  5 push(ba, loc1-3, loc1-2)
%  6 move(loc2-3)
%  7 move(loc3-3)
%  8 move(loc3-2)
%  9 move(loc3-1)
% 10 push(bc, loc2-1, loc1-1)
% 11 push(bb, loc2-2, loc2-3)
% 12 move(loc3-2)
% 13 move(loc3-3)
% 14 push(bb, loc2-3, loc1-3)
% 15 move(loc2-4)



% --- Load domain definitions from an external file -------------------

:- [domain-task7].



% --- Definition of the initial state ---------------------------------

connected(loc1-1, up, loc1-2).
connected(loc1-1, right, loc2-1).
connected(loc1-2, up, loc1-3).
connected(loc1-2, down, loc1-1).
connected(loc1-2, right, loc2-2).
connected(loc1-3, up, loc1-4).
connected(loc1-3, down, loc1-2).
connected(loc1-3, right, loc2-3).
connected(loc1-4, down, loc1-3).
connected(loc1-4, right, loc2-4).
connected(loc2-1, up, loc2-2).
connected(loc2-1, left, loc1-1).
connected(loc2-1, right, loc3-1).
connected(loc2-2, up, loc2-3).
connected(loc2-2, down, loc2-1).
connected(loc2-2, left, loc1-2).
connected(loc2-2, right, loc3-2).
connected(loc2-3, up, loc2-4).
connected(loc2-3, down, loc2-2).
connected(loc2-3, left, loc1-3).
connected(loc2-3, right, loc3-3).
connected(loc2-4, down, loc2-3).
connected(loc2-4, left, loc1-4).
connected(loc3-1, up, loc3-2).
connected(loc3-1, left, loc2-1).
connected(loc3-2, up, loc3-3).
connected(loc3-2, down, loc3-1).
connected(loc3-2, left, loc2-2).
connected(loc3-3, down, loc3-2).
connected(loc3-3, left, loc2-3).
agent(ag).
box(ba).
box(bb).
box(bc).
at(ag, loc3-2, s0).
at(ba, loc2-3, s0).
at(bb, loc2-2, s0).
at(bc, loc2-1, s0).



% --- Goal condition that the planner will try to reach ---------------

goal(S) :-
    (
        at(ba, loc1-2, S),
        at(bb, loc1-3, S),
        at(bc, loc1-1, S),
        
        % odd(ag, S) % comment out to only give even length plans
        not(odd(ag, S)) % comment out to only give odd length plans
    ).



% ---------------------------------------------------------------------
% ---------------------------------------------------------------------
