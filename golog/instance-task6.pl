% ---------------------------------------------------------------------
%  ----- Informatics 2D - 2011/12 - Second Assignment - Planning -----
% ---------------------------------------------------------------------
% Terminates in 0.819s
%  1 move(loc1-4)
%  2 pickup(kc)
%  3 move(loc1-3)
%  4 pickup(kb)
%  5 push(bc, loc1-2, loc1-1)
%  6 push(bb, loc2-2, loc3-2)
%  7 move(loc2-1)
%  8 pickup(ka)
%  9 move(loc2-2)
% 10 move(loc1-2)
% 11 move(loc1-3)
% 12 push(ba, loc2-3, loc3-3)



% --- Load domain definitions from an external file -------------------

:- [domain-task6].



% --- Definition of the initial state ---------------------------------

connected(loc1-1, up, loc1-2).
connected(loc1-1, right, loc2-1).
connected(loc1-2, up, loc1-3).
connected(loc1-2, down, loc1-1).
connected(loc1-2, right, loc2-2).
connected(loc1-3, up, loc1-4).
connected(loc1-3, down, loc1-2).
connected(loc1-3, right, loc2-3).
connected(loc1-4, down, loc1-3).
connected(loc1-4, right, loc2-4).
connected(loc2-1, up, loc2-2).
connected(loc2-1, left, loc1-1).
connected(loc2-1, right, loc3-1).
connected(loc2-2, up, loc2-3).
connected(loc2-2, down, loc2-1).
connected(loc2-2, left, loc1-2).
connected(loc2-2, right, loc3-2).
connected(loc2-3, up, loc2-4).
connected(loc2-3, down, loc2-2).
connected(loc2-3, left, loc1-3).
connected(loc2-3, right, loc3-3).
connected(loc2-4, down, loc2-3).
connected(loc2-4, left, loc1-4).
connected(loc3-1, up, loc3-2).
connected(loc3-1, left, loc2-1).
connected(loc3-2, up, loc3-3).
connected(loc3-2, down, loc3-1).
connected(loc3-2, left, loc2-2).
connected(loc3-3, down, loc3-2).
connected(loc3-3, left, loc2-3).
agent(ag).
box(ba).
box(bb).
box(bc).
key(ka, ba).
key(kb, bb).
key(kc, bc).
at(ag, loc2-4, s0).
at(ba, loc2-3, s0).
at(bb, loc2-2, s0).
at(bc, loc1-2, s0).
at(ka, loc2-1, s0).
at(kb, loc1-3, s0).
at(kc, loc1-4, s0).



% --- Goal condition that the planner will try to reach ---------------

goal(S) :-
    (
        at(ba, loc3-3, S),
        at(bb, loc3-2, S),
        at(bc, loc1-1, S)
    ).



% ---------------------------------------------------------------------
% ---------------------------------------------------------------------
