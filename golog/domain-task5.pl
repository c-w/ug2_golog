% ---------------------------------------------------------------------
%  ----- Informatics 2D - 2011/12 - Second Assignment - Planning -----
% ---------------------------------------------------------------------



% --- Cross-file definitions ------------------------------------------

:- multifile connected/3, agent/1, box/1, at/3.



% --- Primitive control actions ---------------------------------------

primitive_action( move(_) ).
primitive_action( push(_, _, _) ).
primitive_action( rest(_) ).



% --- Precondition for primitive actions ------------------------------
% Axioms now use less variables to improve performance
% Some actions got bound to constants to avoid checks to improve performance

poss( move(To), S) :-
    (
        at(ag, From, S),
        connected(From, _, To),
        not(at(_, To, S))
    ).

poss( push(Box, LocBoxFrom, LocBoxTo), S) :-
    (
        at(ag, LocAgent, S),
        not(tired(ag, S)), % the agent can't push boxes if he is tired
        box(Box),
        at(Box, LocBoxFrom, S),
        connected(LocAgent, Direction, LocBoxFrom),
        connected(LocBoxFrom, Direction, LocBoxTo),
        not(at(_, LocBoxTo, S))
    ).

% the agent can rest if he is tired
poss( rest(ag), S) :-
    (
        tired(ag, S)
    ).



% --- Successor state axioms ------------------------------------------

tired(ag, result(A, S)) :-
    (
        % the agent is tired if he pushed
        A=push(_, _, _);
        (
            % or if he did not move or rest and was tired before
            not(A=move(_)),
            not(A=rest(_)),
            tired(ag, S)
        )
    ).

at(Who, Loc, result(A, S)) :-
    (
        (
            agent(Who), 
            (
                A=move(Loc);
                A=push(_, Loc, _);
                (
                    at(Who, Loc, S),
                    not(A=move(_)),
                    not(A=push(_, _, _))
                )
            )
        );
        (
            box(Who),
            (
                A=push(Who, _, Loc);
                (
                    at(Who, Loc, S),
                    not(A=push(Who, _, _))
                )
            )
        )
    ).



% ---------------------------------------------------------------------
% ---------------------------------------------------------------------
