% ---------------------------------------------------------------------
%  ----- Informatics 2D - 2011/12 - Second Assignment - Planning -----
% ---------------------------------------------------------------------
% Terminates in 0.871s                                start at  loc1-1
%  1 move(a, loc1-1, up, loc1-2)                      go to     loc1-2
%  2 move(a, loc1-2, up, loc1-3)                      go to     loc1-3
%  3 move(a, loc1-3, up, loc1-4)                      go to     loc1-4
%  4 move(a, loc1-4, right, loc2-4)                   go to     loc2-4
%  5 push(a, loc2-4, bb, loc2-3, down, loc2-2)        pushgo to loc2-3
%  6 move(a, loc2-3, right, loc3-3)                   go to     loc3-3
%  7 move(a, loc3-3, down, loc3-2)                    go to     loc3-2
%  8 push(a, loc3-2, bb, loc2-2, left, loc1-2)        pushgo to loc2-2
%  9 move(a, loc2-2, right, loc3-2)                   go to     loc3-2
% 10 move(a, loc3-2, down, loc3-1)                    go to     loc3-1
% 11 push(a, loc3-1, ba, loc2-1, left, loc1-1)        pushgo to loc2-1
%
% Initial state:
% [   ] [   ]
% [   ] [ B ] [   ]
% [ T ] [   ] [   ]
% [@ t] [ b ] [   ]
%
% @ is the agent, b and B are boxes
% t is the target position of box b, T is the target position of box B



% --- Load domain definitions from an external file -------------------

:- [domain-task4].



% --- Definition of the initial state ---------------------------------

connected(loc1-1, up, loc1-2).
connected(loc1-1, right, loc2-1).
connected(loc1-2, up, loc1-3).
connected(loc1-2, down, loc1-1).
connected(loc1-2, right, loc2-2).
connected(loc1-3, up, loc1-4).
connected(loc1-3, down, loc1-2).
connected(loc1-3, right, loc2-3).
connected(loc1-4, down, loc1-3).
connected(loc1-4, right, loc2-4).
connected(loc2-1, up, loc2-2).
connected(loc2-1, left, loc1-1).
connected(loc2-1, right, loc3-1).
connected(loc2-2, up, loc2-3).
connected(loc2-2, down, loc2-1).
connected(loc2-2, left, loc1-2).
connected(loc2-2, right, loc3-2).
connected(loc2-3, up, loc2-4).
connected(loc2-3, down, loc2-2).
connected(loc2-3, left, loc1-3).
connected(loc2-3, right, loc3-3).
connected(loc2-4, down, loc2-3).
connected(loc2-4, left, loc1-4).
connected(loc3-1, up, loc3-2).
connected(loc3-1, left, loc2-1).
connected(loc3-2, up, loc3-3).
connected(loc3-2, down, loc3-1).
connected(loc3-2, left, loc2-2).
connected(loc3-3, down, loc3-2).
connected(loc3-3, left, loc2-3).
agent(a).
box(ba).
box(bb).
at(a, loc1-1, s0).
at(ba, loc2-1, s0).
at(bb, loc2-3, s0).



% --- Goal condition that the planner will try to reach ---------------

goal(S) :-
    (
        at(ba, loc1-1, S),
        at(bb, loc1-2, S)
    ).



% ---------------------------------------------------------------------
% ---------------------------------------------------------------------
