% ---------------------------------------------------------------------
%  ----- Informatics 2D - 2011/12 - Second Assignment - Planning -----
% ---------------------------------------------------------------------



% --- Cross-file definitions ------------------------------------------

% :- multifile connected/3, agent/1, box/1, at/3.



% --- Primitive control actions ---------------------------------------

primitive_action( move(_, _, _, _) ).
primitive_action( push(_, _, _, _, _, _) ).



% --- Precondition for primitive actions ------------------------------
% Don't need empty: empty(l, s) <=> for_all x: -at(x, l, s)

poss( move(Who, From, Direction, To), S) :-
    (
        agent(Who),
        at(Who, From, S),
        connected(From, Direction, To), 
        not(at(_, To, S))
    ).

poss( push(Agent, LocAgent, Box, LocBoxFrom, Direction, LocBoxTo), S) :-
    (
        agent(Agent),
        at(Agent, LocAgent, S),
        box(Box),
        at(Box, LocBoxFrom, S),
        connected(LocAgent, Direction, LocBoxFrom),
        connected(LocBoxFrom, Direction, LocBoxTo),
        not(at(_, LocBoxTo, S))
    ).



% --- Successor state axioms ------------------------------------------

at(Who, Loc, result(A, S)) :-
    (
        (
            agent(Who), 
            (
                A=move(Who, _, _, Loc);
                A=push(Who, _, _, Loc, _, _);
                (
                    at(Who, Loc, S),
                    not(A=move(Who, Loc, _, _)),
                    not(A=push(Who, Loc, _, _, _, _))
                )
            )
        );
        (
            box(Who),
            (
                A=push(_, _, Who, _, _, Loc);
                (
                    at(Who, Loc, S),
                    not(A=push(_, _, Who, Loc, _, _))
                )
            )
        )
    ).



% ---------------------------------------------------------------------
% ---------------------------------------------------------------------
